var app = angular.module('myApp', []);

// NPR API station list 
var apiKey = 'MDMxMTE0NzYxMDE0ODg4Mjk1OTZhN2M0OQ000';
var nprUrl = 'https://api.npr.org/query?orgId=936,552&fields=title,byline,text,audio,image,pullQuote,all&output=JSON';


//Creates audio element
app.factory('audio', ['$document', function($document) {
  var audio = $document[0].createElement('audio');
  return audio;
}]);

//Manages the audio element
app.factory('player', ['audio', '$rootScope', function(audio, $rootScope) {
  var player = {
    playing: false,
    current: null,
    ready: false,
    
    currentTime: function() {
      return audio.currentTime;
    },
    currentDuration: function() {
      return parseInt(audio.duration);
    },
    
    play: function(program) {
      //if we are playing, stop the current playback
      if(player.playing) player.stop();
      var url = program.audio[0].format.mp4.$text; //from the npr API
      player.current = program; //stores the current program
      audio.src= url;
      audio.play(); //Start the playback of the url
      player.playing = true;
    },
    
    stop: function() {
      if(player.playing) {
        audio.pause(); //stop the playback
        //clear the state of the player
        player.ready = player.playing = false;
        player.current = null;
      }
    }
  };
  audio.addEventListener('ended', function() {
      $rootScope.$apply(player.stop());
  });
  
  audio.addEventListener('timeupdate', function(evt) {
    $rootScope.$apply(function() {
      player.progress = player.currentTime();
      player.progress_percent = player.progress / player.currentDuration();
    });
  });
  
  audio.addEventListener('canplay', function(evt) {
    $rootScope.$apply(function() {
      player.ready = true;
    });
  });
  return player;
}]);

//HTTPS request service
app.factory('nprService', ['$http', function($http) {
  var doRequest = function(apiKey) {
    return $http({
      method: 'JSONP',
      url: nprUrl + '&apiKey=' + apiKey + '&callback=JSON_CALLBACK'
    });
  };
  
  return {
    programs: function(apiKey) { return doRequest(apiKey); }
  };
}]);

//Directive
app.directive('nprLink', function() {
  return {
    restrict: 'EA',
    require: ['^ngModel'],
    replace: true,
    scope: {
      ngModel: '=',
      player: '='
    },
    templateUrl: '/cblair-angular/views/nprListItem.html',
    link: function(scope,ele,attr) {
      scope.duration = scope.ngModel.audio[0].duration.$text;
    }
  };
});



//Player Controller
app.controller('PlayerController', ['$scope','nprService', 'player',
  function($scope, nprService, player) {
    $scope.player = player;
    nprService.programs(apiKey)
      .success(function(data,status) {
        $scope.programs = data.list.story;
      });
}]);




//Related Controller for sidebar
app.controller('RelatedController', ['$scope', 'player',
  function($scope, player) {
  $scope.player = player;
 
  $scope.$watch('player.current', function(program) {
    if (program) {
      $scope.related = [];
      angular.forEach(program.relatedLink, function(link) {
        $scope.related.push({
          link: link.link[0].$text, 
          caption: link.caption.$text
        });
      });
    }
  });
}]);

